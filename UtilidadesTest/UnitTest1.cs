﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Proyecto01Calidad;

namespace UtilidadesTest
{
    [TestClass]
    public class utilidadesTest
    {
        [TestMethod]
        public void TestDivision()
        {
            //Setup
            Utilidades u = new Utilidades();
            int dividendo = 10;
            int divisor = 2;
            int esperado = 5;
            int actual;
            //ejecucion
            actual = u.division(dividendo, divisor);
            //assert
            Assert.AreEqual(esperado, actual);
        }
        [TestMethod]
        public void TestExtraerNumero()
        {
            //setup
            string cadena = "hola, soy 1 cadena";
            int esperado = 1;
            int actual;
            Utilidades u = new Utilidades();
            //ejecución
            actual = u.extraerNumero(cadena);
            //assert
            Assert.AreEqual(esperado, actual);
        }
    }
}
