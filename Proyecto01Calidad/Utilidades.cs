﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto01Calidad
{
    public class Utilidades
    {
        public Utilidades()
        {

        }
        public int division(int dividendo, int divisor)
        {
            if (divisor == 0)
                return 0;
            return (dividendo / divisor);
        }
        public int extraerNumero(string cadena)
        {
            int retorno;
            foreach(char c in cadena)
            {
                retorno = Convert.ToInt32(c);
                if(retorno >= 48 && retorno <= 57)
                {
                    return int.Parse(c.ToString());
                }
            }
            return 10;
        }
    }
}
