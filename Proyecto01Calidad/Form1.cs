﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto01Calidad
{
    public partial class Form1 : Form
    {
        private Utilidades u;
        public Form1()
        {
            InitializeComponent();
            u = new Utilidades();
        }

        private void btnDividir_Click(object sender, EventArgs e)
        {
            int i1;
            int i2;
            try
            {
                i1 = Convert.ToInt32(txtDividendo.Text);
            }
            catch (Exception)
            {
                throw;
            }
            try
            {
                i2 = Convert.ToInt32(txtDivisor.Text);
            }
            catch (Exception)
            {
                throw;
            }
            txtResultados.Text = u.division(i1, i2).ToString();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            txtResultados.Text = u.extraerNumero(txtString.Text).ToString();

        }
    }
}
